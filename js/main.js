var openPopupBtn = document.getElementById('openPopupBtn');
var closePopupButton = document.getElementById('closePopupButton');
var popup = document.querySelector('.popup');
var popupback = document.querySelector('.popup-back');

openPopupBtn.addEventListener('click', openPopup);
closePopupButton.addEventListener('click', closePopup);
popupback.addEventListener('click', closePopup);


function openPopup() {
  popup.classList.remove('popup-hidden');
}

// #TODO
function closePopup() {
  popup.classList.add('popup-hidden');
}
